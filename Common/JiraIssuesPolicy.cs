﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using Microsoft.TeamFoundation.VersionControl.Client;

using Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper;

namespace Spartez.TFS4JIRA.CheckInPolicy.JiraIssuesPolicy
{
    [Serializable]
    public class JiraIssuesPolicy : PolicyBase
    {        
        public string JiraURL = "";
        public bool ScanCheckInComment = true;
        public bool ScanCheckInNote = false;
        public string ScannedCheckInNoteName = "";

        [NonSerialized] private JiraIssuesPolicyHelper jiraIssuesPolicyHelper;

        public override bool CanEdit { get { return true; } }
        public override string Description { get { return "TFS4JIRA SPARTEZ JIRA Issue Keys Policy"; } }
        public override string Type { get { return "JIRA Issue Key Policy"; } }
        public override string TypeDescription { get { return "This policy prevents users from checking in without associated issue keys"; } }
        

        public override bool Edit(IPolicyEditArgs policyEditArgs)
        {
            if (jiraIssuesPolicyHelper == null)
            {
                jiraIssuesPolicyHelper = new JiraIssuesPolicyHelper();
            }

            IEnumerable<string> checkInNoteFieldNames = from note in policyEditArgs.TeamProject.GetCheckinNoteFields() select note.Name;

            GlobalCheckInPolicySettings globalCheckInPolicySettings = GetGlobalPolicySettings();

            if (jiraIssuesPolicyHelper.Edit(globalCheckInPolicySettings, checkInNoteFieldNames))
            {
                JiraURL = globalCheckInPolicySettings.JiraUrl;
                ScanCheckInComment = globalCheckInPolicySettings.ScanCheckInComment;
                ScanCheckInNote = globalCheckInPolicySettings.ScanCheckInNote;
                ScannedCheckInNoteName = globalCheckInPolicySettings.ScannedCheckInNoteName;

                return true;
            }

            return false;
        }

        public override PolicyFailure[] Evaluate()
        {
            IEnumerable<Tuple<string, string>> checkinNotes = new Tuple<string, string>[] {};
            if (PendingCheckin.CheckinNotes.CheckinNotes != null)
            {
                checkinNotes = from note in PendingCheckin.CheckinNotes.CheckinNotes.Values select new Tuple<string, string>(note.Name, note.Value);
            }

            var result = from failure in jiraIssuesPolicyHelper.Evaluate(GetGlobalPolicySettings(), PendingCheckin.PendingChanges.Comment, checkinNotes)
                         select new JiraIssuesPolicyFailure(failure, this);

            return result.Cast<PolicyFailure>().ToArray();
        }

        public override void Activate(PolicyFailure arg)
        {
            JiraIssuesPolicyFailure failure = arg as JiraIssuesPolicyFailure;

            if (failure != null && jiraIssuesPolicyHelper.Activate(GetGlobalPolicySettings(), failure.JiraPolicyFailureDetails))
                base.OnPolicyStateChanged(Evaluate());
        }

        public override void DisplayHelp(PolicyFailure arg)
        {
            JiraIssuesPolicyFailure failure = arg as JiraIssuesPolicyFailure;

            if (failure != null)
            {
                jiraIssuesPolicyHelper.DisplayHelp(failure.JiraPolicyFailureDetails);
            }
        }

        public override void Initialize(IPendingCheckin pendingCheckin)
        {
            base.Initialize(pendingCheckin);

            if (jiraIssuesPolicyHelper == null)
            {
                jiraIssuesPolicyHelper = new JiraIssuesPolicyHelper();
            }
        }

        private GlobalCheckInPolicySettings GetGlobalPolicySettings()
        {
            return new GlobalCheckInPolicySettings()
            {
                JiraUrl = JiraURL,
                ScanCheckInComment = ScanCheckInComment,
                ScanCheckInNote = ScanCheckInNote,
                ScannedCheckInNoteName = ScannedCheckInNoteName
            };
        }
    }
}
