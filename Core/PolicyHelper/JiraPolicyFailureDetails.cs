﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Spartez.TFS4JIRA.CheckInPolicy.PolicyHelper
{
    public enum JiraPolicyFailureType
    {
        CONFIGURATION_ERROR,
        NO_JIRA_URL,
        NO_CREDENTIALS,
        NO_ISSUE_KEYS,
        ISSUE_NOT_FOUND
    }

    public class JiraPolicyFailureDetails
    {
        public static JiraPolicyFailureDetails GetJiraPolicyFailureDetails(JiraPolicyFailureType failureType, string message)
        {
            return new JiraPolicyFailureDetails()
            {
                JiraUrl = null,
                FailureType = failureType,
                Message = message,
                IssueKey = null,
            };
        }

        public static JiraPolicyFailureDetails GetJiraPolicyFailureDetailsForIssue(JiraPolicyFailureType failureType, string message, string issueKey)
        {
            return new JiraPolicyFailureDetails()
            {
                JiraUrl = null,
                FailureType = failureType,
                Message = message,
                IssueKey = issueKey,
            };
        }

        public static JiraPolicyFailureDetails GetJiraPolicyFailureDetailsForJira(JiraPolicyFailureType failureType, string message, string jiraUrl)
        {
            return new JiraPolicyFailureDetails()
            {
                JiraUrl = jiraUrl,
                FailureType = failureType,
                Message = message,
                IssueKey = null,
            };
        }

        public JiraPolicyFailureType FailureType { get; private set; }
        public string IssueKey { get; private set; }
        public string Message { get; private set; }
        public string JiraUrl { get; private set; }
        
    }
}
